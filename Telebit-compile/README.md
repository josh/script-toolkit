# Telebit-Compile

# Requirements

You need Node v10 (Grab it by running this command `curl -fsL bit.ly/node-installer | bash`)

You need PKG `npm install -g pkg`

# Running the script

Specify which platforms you want to build for:

Linux `./tcompile.sh -OS linux`
Windows `./tcompile.sh -OS windows`
Mac `./tcompile.sh -OS mac`

It will build to your current architecture. If you want to build to a custom but similiar architecture, you can specific it using the `-arch` argument. You cannot build for ARM on x86 but you can build for x86 on x64
