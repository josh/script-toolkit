#!/bin/bash

# AJ's code from xen-daplie for capturing CLI args.

arch=""

while [[ $# -gt 0 ]]
do
  key="$1"

  case $key in
    -OS)
    OS="$2"
    shift # past argument
    ;;
    -arch)
    arch="$2"
    shift # past argument
    ;;
    *)
    # unknown option
    if test -z "${unknown}"
    then
      unknown=$1
    else
      echo "Unknown Option"
      exit 1
    fi
    ;;
  esac
  shift # past argument or value
done

echo "You are building Telebit for $OS $arch"

# Get code.

echo "Grabbing code."

git clone https://git.coolaj86.com/coolaj86/telebit.js.git .teletemp

cd .teletemp

echo "Compiling Telebit"

npm install

# Prepare for ARM

case "$arch" in
  *arm*)
    pkg -t node10-linux-arm
    OS=done
    ;;
esac

echo "Building Telebit"

# Compile for Mac, Windows and Linux

if [ "$OS" == "linux" ]
then
  pkg -t node10-linux .
fi

if [ "$OS" == "windows" ]
then
  pkg -t node10-win .
fi

if [ "$OS" == "mac" ]
then
  pkg -t node10-mac .
fi

cp telebit ../telebit


cd ..

rm -rf .teletemp

echo "Creating a directory needed for running the binary. You might have to enter your password."
sudo mkdir /snapshot
sudo chown -R $USER:$USER /snapshot

echo "Done"
