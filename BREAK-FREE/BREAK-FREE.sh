#!/bin/bash
# Telebit Break Free v1.0
# Break free from bad internet filters and censorship using Telebit: https://git.coolaj86.com/coolaj86/telebit.js
# This script allows you to use SSH and a free web over networks that have blocked everything other than port 443.

# Requirements:
#   A device with Telebit installed (a Raspberry pi or a Linux PC will do nicely)
#   Put `#ProxyCommand=nc -X 5 -x localhost:8080 %h %p` in ~/.ssh/config

# It will setup a SOCKS proxy on port 8080 that will forward through whatever device you are using for the tunnel.
# When you are done with the tunnel, just type `exit` and hit enter. This script will take care of the rest.

sed -i 's/#//' ~/.ssh/config # Uncomment this line: #ProxyCommand=nc -X 5 -x localhost:8080 %h %p to use Telebit SSH as a proxy for all SSH requests. http://www.theunixschool.com/2014/08/sed-examples-remove-delete-chars-from-line-file.html
ssh -D 8080 -C -o ProxyCommand='openssl s_client -connect %h:443 -servername %h -quiet' grumpy-cheetah-21.telebit.website
sed -i '1s/^/#/' ~/.ssh/config # We're done, no need to proxy everything through Telebit SSH. https://stackoverflow.com/questions/9533679/how-to-insert-a-text-at-the-beginning-of-a-file
